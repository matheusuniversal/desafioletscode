using System.Threading.Tasks;
using BACK.Models;
using BACK.Repositories;
using BACK.Services;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;

namespace BACK.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class LoginController : ControllerBase
    {
        private readonly IConfiguration _config;
        public LoginController(IConfiguration config)
        {
            _config = config;
        }
        [HttpPost]
        public async Task<ActionResult<dynamic>> Authenticate(User usuario)
        {
            var userRepository = new UserRepository(_config);
            var user = userRepository.Get(usuario.Login, usuario.Senha);

            if (user == null)
            {
                return Unauthorized();
            }

            var tokenService = new TokenService(_config);
            var token = tokenService.GenerateToken(user);
            return new { token = token };
        }
    }
}