using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BACK.Context;
using BACK.Filter;
using BACK.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;

namespace BACK.Controllers
{

    [ApiController]
    [Route("cards")]
    public class CardController : ControllerBase
    {
        private ApiContext _apiContext;
        public CardController(ApiContext context)
        {

        }

        [Authorize]
        [HttpGet]

        public async Task<ActionResult<List<Card>>> GetCard([FromServices] ApiContext context)
        {
            var cards = await context.Cards.ToListAsync();
            return cards;
        }


        [HttpPost]
        [Authorize]

        public IActionResult PostCard([FromServices] ApiContext context, Card card)
        {
            context.Cards.Add(card);
            context.SaveChanges();

            return Ok(card);
        }

        [HttpPut("{id}")]
        [Authorize]
        [ServiceFilter(typeof(ActionFilter))]
        public IActionResult PutCard([FromServices] ApiContext context, string id, Card newCard)
        {

            var card = context.Cards.Where(x => x.Id.ToString() == id).FirstOrDefault();
            if (card == null)
            {
                return NotFound();
            }

            card.Titulo = newCard.Titulo;
            card.Conteudo = newCard.Conteudo;
            card.Lista = newCard.Lista;

            context.Cards.Update(card);
            context.SaveChanges();


            return StatusCode(200);
        }

        [HttpDelete("{id}")]
        [Authorize]
        [ServiceFilter(typeof(ActionFilter))]
        public IActionResult DeleteCard([FromServices] ApiContext context, string id)
        {

            var card = context.Cards.Where(x => x.Id.ToString() == id).FirstOrDefault();
            if (card == null)
            {
                return NotFound();
            }



            context.Cards.Remove(card);
            context.SaveChanges();


            return StatusCode(200);
        }


    }
}