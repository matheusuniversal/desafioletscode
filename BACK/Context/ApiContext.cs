using BACK.Models;
using Microsoft.EntityFrameworkCore;

namespace BACK.Context
{
    public class ApiContext : DbContext
    {
        public ApiContext(DbContextOptions<ApiContext> options) : base(options)
        {

        }

        public DbSet<Card> Cards { get; set; }
    }
}