using System;
using System.Linq;
using System.Reflection;
using BACK.Context;
using BACK.Models;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.DependencyInjection;

namespace BACK.Filter
{
    public class ActionFilter : IActionFilter
    {
        public void OnActionExecuted(ActionExecutedContext context)
        {
            Console.WriteLine("OnActionExecuted");
        }


        public void OnActionExecuting(ActionExecutingContext context)
        {
            var id = context.HttpContext.Request.RouteValues["id"];
            var method = context.HttpContext.Request.Method;
            var _context = context.HttpContext.RequestServices.GetService<ApiContext>();
            var card = _context.Cards.Where(x => x.Id.ToString() == id.ToString()).FirstOrDefault();
            if (card != null)
            {
                string tipo = "";
                if (method == "PUT") tipo = "Alterar";
                if (method == "DELETE") tipo = "Remover";
                if (method == "PUT" || method == "DELETE")
                    Console.WriteLine($"{DateTime.Now} - Card {id} - {card.Titulo} - {tipo}");
            }
        }
    }
}