using System;
using System.ComponentModel.DataAnnotations;

namespace BACK.Models
{
    public class Card
    {

        [Key]
        public Guid Id { get; set; } = Guid.NewGuid();

        [Required(ErrorMessage = "Titulo is required")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "Conteudo is required")]
        public string Conteudo { get; set; }

        [Required(ErrorMessage = "Lista is required")]
        public string Lista { get; set; }
    }
}