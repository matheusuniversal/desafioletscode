using System.Collections.Generic;
using System.Linq;
using BACK.Models;
using Microsoft.Extensions.Configuration;

namespace BACK.Repositories
{

    public class UserRepository
    {
        private readonly IConfiguration _config;

        public UserRepository(IConfiguration config)
        {
            _config = config;
        }
        public User Get(string login, string senha)
        {
            var users = new List<User>();

            users.Add(new User
            {
                Login = _config.GetSection("ApiConfig").GetValue<string>("login"),
                Senha = _config.GetSection("ApiConfig").GetValue<string>("senha")
            });

            return users.Where(x => x.Login.ToLower() == login.ToLower() && x.Senha.ToLower() == senha.ToLower()).FirstOrDefault();

        }
    }
}